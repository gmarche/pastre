<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AuthController extends AbstractController
{

    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $response = new JsonResponse();
        $email = $request->get('email');
        if (is_null($this->userRepository->findOneByEmail($email))) {
            $password = $request->get('password');
            $user = $this->userRepository->createNewUser($email, $password, $passwordEncoder);
            $response->setData(['id' => $user->getId(), 'email' => $user->getEmail()]);
        } else {
            $response->setStatusCode(403);
        }

        return $response;
    }
}