<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
        $this->manager = $registry->getManager();
    }

    public function getEntityName()
    {
        return User::class;
    }

    public function createNewUser(string $email, string $password, UserPasswordEncoderInterface $passwordEncoder): User
    {
        $user = new User();
        $user->setEmail($email);
        $user->setPassword(
            $passwordEncoder->encodePassword(
                $user,
                $password
            )
        );
        $this->manager->persist($user);
        $this->manager->flush();

        return $user;
    }

    public function findOneByEmail(string $email): ?User
    {
        $queryBuilder = $this->createQueryBuilder('u');
        $queryBuilder->where('u.email = :email')->setParameter('email', $email);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}