export const REGISTER_SUCCESSFUL = "REGISTER_SUCCESSFUL";
export const REGISTER_FAIL = "REGISTER_FAIL";
export const REGISTER_FORM_CHANGE = "REGISTER_FORM_CHANGE";