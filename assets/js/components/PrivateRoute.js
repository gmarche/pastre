import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import {useSelector} from "react-redux";

export const PrivateRoute = ({ component: Component, location: location, ...rest }) => {
    const {loggedIn} = useSelector(state => state.authentication);
    if (!loggedIn) {
        return <Redirect to={{ pathname: '/login', state: { from: location } }} />
    }

    return (
        <Route {...rest} render={props => (
            <Component {...props} />
        )} />
    )
}