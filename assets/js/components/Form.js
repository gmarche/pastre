import React, {Component} from 'react';

const Label = ({text}) => {
    return (
        <label>{text}</label>
    )
}

export const TextInput = ({error, label, onChange, placeholder, name}) => {
    return (
        <div className="form-group">
            <Label text={label}/>
            <input className="form-control" type={['password', 'confirmPassword'].includes(name) ? "password" : "text"} name={name} placeholder={placeholder} onChange={onChange} />
            {error && <div className="col-md-12 invalid-feedback d-block">{error}</div>}
        </div>
    )
}

export const Button = ({onClick, label, action}) => {
    return (
        <button
            className="btn btn-primary mt-2"
            disabled={action}
            onClick={onClick}>
            {action && <span className="spinner-border spinner-border-sm mr-1" />}
            <span>{label}</span>
        </button>
    )
}
