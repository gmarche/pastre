import React from "react";
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux'
import {withRouter} from 'react-router-dom';

export function NavBar() {
    const {loggedIn, user} = useSelector(state => state.authentication);
    if (!loggedIn) {
        return null;
    }
    return <Bar roles={user.roles} />
}

function Bar({roles}) {

    const menu = [
        {
            path: '/',
            isAdmin: false,
            label: 'Pastre',
            name: "home"
        },
        {
            path: '/admin',
            isAdmin: true,
            label: 'Admin',
            name: "admin"
        },
        {
            path: '/About',
            isAdmin: true,
            label: 'About',
            name: "About"
        },
        {
            path: '/login',
            isAdmin: false,
            label: 'Déconnexion',
            name: "logout"
        }
    ]

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <ResponsiveButton />
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                    {
                        menu.map((row, key) => {
                            return <BarButton {...row} roles={roles} key={key}/>
                        })
                    }
                </ul>
            </div>
        </nav>
    )
}

const BarButton = ({label, isAdmin, path, roles}) => {

    if (isAdmin && !roles.includes('ROLE_ADMIN')) {
        return null;
    }

    return (
        <li className={"nav-item" + (location.pathname === path ? " active" : "")}>
            <Link className="nav-link" to={path}>{label}</Link>
        </li>
    )
}

const ResponsiveButton = () => {
    return (
        <button className="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"/>
        </button>
    )
}