import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import {useSelector} from "react-redux";

export const AdminRoute = ({ component: Component, location: location, ...rest }) => {

    const {loggedIn, user} = useSelector(state => state.authentication);

    if (!loggedIn || !user.roles.includes("ROLE_ADMIN")) {
        return <Redirect to={{ pathname: '/login', state: { from: location } }} />
    }

    return (
        <Route {...rest} render={props => (
            <Component {...props} />
        )} />
    )
}