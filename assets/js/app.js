import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';

import { store } from './helpers/store';
import '../css/app.css';
import {App} from './containers/App/App';

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));