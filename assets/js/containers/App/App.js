import React, {useEffect} from 'react';
import {Router, Route, Switch, Redirect, Link} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';

import {history} from '../../helpers/history';
import {alertActions} from '../../actions/alert.actions';
import {PrivateRoute} from '../../components/PrivateRoute';
import {AdminRoute} from '../../components/AdminRoute';
import {NavBar} from '../../components/NavBar';
import {Home} from '../Home/Home';
import {Login} from '../Login/Login';
import {Register} from '../Register/Register';
import {Admin} from '../Admin/Admin';


function App() {
    const alert = useSelector(state => state.alert);
    const dispatch = useDispatch();

    useEffect(() => {
        history.listen((location, action) => {
            dispatch(alertActions.clear());
        });
    }, []);

    return (
        <Router history={history}>
            <div className={"container-fluid pl-0 pr-0"}>
                <NavBar/>
                <div className="container">
                    <div className="col-md-12">
                        {alert.message &&
                        <div className={`alert ${alert.type}`}>{alert.message}</div>
                        }
                        <Switch>
                            <PrivateRoute exact path="/" component={Home}/>
                            <Route path="/login" component={Login}/>
                            <Route path="/register" component={Register}/>
                            <PrivateRoute path='/admin' component={Admin} />
                            <Redirect from="*" to="/"/>
                        </Switch>
                    </div>
                </div>
            </div>
        </Router>
    );
}

export {App};