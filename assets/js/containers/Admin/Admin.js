import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {Link, Route} from "react-router-dom";

const Paragraph = ({ para }) => (
    <p>{para}</p>
)

export function Admin({match}) {

    const CONTENT = {
        FirstPage: 'This is the content for the first Page',
        SecondPage: 'This is the content for the second Page',
        ThirdPage: 'This is the content for the third Page',
    }

    return (
        <div className={"col-md-12"}>
            <h2>Admin</h2>
            <ul>
                {Object.keys(CONTENT).map(page => <li key={page}><Link to={`${match.url}/${page}`}>{page}</Link></li>)}
            </ul>
            {Object.keys(CONTENT).map(page => <Route path={`${match.path}/${page}`} render={() => <Paragraph para={CONTENT[page]}/>}/>)}
        </div>
    )
}