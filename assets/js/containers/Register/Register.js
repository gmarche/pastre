import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { userActions } from '../../actions/user.action';
import {TextInput, Button} from "../../components/Form";

function Register() {
    const [user, setUser] = useState({
        email: '',
        password: '',
        confirmPassword: '',
    });

    const [submitted, setSubmitted] = useState(false);
    const registering = useSelector(state => state.registration.registering);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(userActions.logout());
    }, []);

    function handleChange(e) {
        const { name, value } = e.target;
        setUser(user => ({ ...user, [name]: value }));
    }

    function handleSubmit(e) {
        e.preventDefault();

        setSubmitted(true);
        if (user.email && user.password && user.confirmPassword && user.password === user.confirmPassword) {
            const {confirmPassword, ...sendData} = user;
            dispatch(userActions.register(sendData));
        }else{
            setSubmitted(false);
        }
    }

    return (
        <div className={"container"}>
            <div className={"row"}>
                <div className={"col-md-8 offset-md-2 mt-5"}>
                    <h2>Inscription</h2>
                    <form onSubmit={handleSubmit}>
                        <TextInput
                            label={"Email"}
                            name={"email"}
                            placeholder={"email"}
                            error={submitted && !user.email && ""}
                            value={user.email}
                            onChange={handleChange}
                        />
                        <TextInput
                            label={"Mot de passe"}
                            name={"password"}
                            placeholder={"mot de passe"}
                            error={submitted && !user.password && ""}
                            value={user.password}
                            onChange={handleChange}
                        />
                        <TextInput
                            label={"Confirmer le Mot de passe"}
                            name={"confirmPassword"}
                            placeholder={"Confirmer le mot de passe"}
                            error={submitted && !user.confirmPassword && ""}
                            value={user.confirmPassword}
                            onChange={handleChange}
                        />
                        <Button label={"Valider"} action={registering}/>
                        <Link to="/login" className="btn btn-link">Annuler</Link>
                    </form>
                </div>
            </div>
        </div>
    )
}

export {Register};