import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { userActions } from '../../actions/user.action';
import {TextInput, Button} from "../../components/Form";

function Login() {

    const [inputs, setInputs] = useState({
        email: '',
        password: ''
    });
    const [submitted, setSubmitted] = useState(false);
    const {email, password} = inputs;
    const loggingIn = useSelector(state => state.authentication.loggingIn);
    const dispatch = useDispatch();

    // reset login status
    useEffect(() => {
        dispatch(userActions.logout());
    }, []);

    function handleChange(e) {
        const {name, value} = e.target;
        setInputs(inputs => ({...inputs, [name]: value}));
    }

    function handleSubmit(e) {
        e.preventDefault();

        setSubmitted(true);
        if (email && password) {
            dispatch(userActions.login(email, password));
        }
    }

    return (
        <div className={"container"}>
            <div className={"row"}>
                <div className={"col-md-6 offset-md-3 mt-5"}>
                    <h2>Connexion</h2>
                    <form onSubmit={handleSubmit}>
                        <TextInput
                            label={"Email"}
                            name={"email"}
                            placeholder={"email"}
                            onChange={handleChange}
                            value={email}
                            error={submitted && email === "" && "Email is required"}
                        />
                        <TextInput
                            label={"Mot de passe"}
                            name={"password"}
                            placeholder={"mot de passe"}
                            onChange={handleChange}
                            value={password}
                            error={submitted && password === "" && "Password is required"}
                        />
                        <Button label={"Valider"} action={loggingIn}/>
                        <Link to="/register" className="btn btn-link">Inscription</Link>
                    </form>
                </div>
            </div>
        </div>
    )
}

export {Login};